<img src="data/Banner_Fly_Cancelled.jpeg" style="width:700px;">


# Prediccion de Cancelacion de un vuelo en el mes de Enero

Este proyecto fue realizado con fines educativos para la materia de Inteligencia Artificial

## Autores
#### Jorge Andres Burgos - 2171984
#### Angel Ronaldo Roa Prada - 2171984
#### Einer Steven Jaimes - 2171978

## Introduccion

Las aerolíneas hoy en día conforman la columna vertebral del sistema de transporte a nivel mundial y tienen una
utilidad socioeconómica significativa al permitir viajes de larga distancia.
Después de más de medio siglo de que EEUU adoptara este sistema de transporte las aerolíneas han visto
optimizaciones importantes llegando a funcionar con un alto rango de confiabilidad, pero también se tienen
que enfrentar a grandes retos como suelen ser las condiciones climáticas más que todo en épocas invernales, 
llegando a ocasionar la cancelación de vuelos generando así perdidas millonarias.

## Objetivo
El objetivo general de este proyecto consiste en analizar la
aplicabilidad de las técnicas de clasificación para solucionar los
problemas de predicción en la cancelación de los vuelos
realizados, en este caso, EEUU.

## Recursos

- **Dataset :** January Flight Delay Prediction años 2019-2020
- **Modelos :** Naive Gaussian Bayes, 
                Decision Tree Classifier,
                Random Forest Classifier,
                Deep Neuronal Network,
                Support Vector Machines.


## Links

* **Link del Video** - [Ver](https://youtu.be/HLt4NZSAPE4)
* **Link de Datasets 2019-2020**  - [Ver](https://www.kaggle.com/divyansh22/flight-delay-prediction)

